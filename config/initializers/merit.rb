# Use this hook to configure merit parameters
Merit.setup do |config|
  # Check rules on each request or in background
  # config.checks_on_each_request = true

  # Define ORM. Could be :active_record (default) and :mongoid
  # config.orm = :active_record

  # Add application observers to get notifications when reputation changes.
  # config.add_observer 'MyObserverClassName'
  config.add_observer 'UserChangeObserver'

  # Define :user_model_name. This model will be used to grand badge if no
  # `:to` option is given. Default is 'User'.
  # config.user_model_name = 'User'

  # Define :current_user_method. Similar to previous option. It will be used
  # to retrieve :user_model_name object if no `:to` option is given. Default
  # is "current_#{user_model_name.downcase}".
  # config.current_user_method = 'current_user'
end

# Create application badges (uses https://github.com/norman/ambry)
# badge_id = 0
# [{
#   id: (badge_id = badge_id+1),
#   name: 'just-registered'
# }, {
#   id: (badge_id = badge_id+1),
#   name: 'best-unicorn',
#   custom_fields: { category: 'fantasy' }
# }].each do |attrs|
#   Merit::Badge.create! attrs
# end
badge_id = 0

# Lost Weight Badges
[
  {
    id: (badge_id = badge_id+1),
    name: 'lost-1kg',
    description: 'Você perdeu 1kg. Como prêmio, essa medalha estará pra sempre com você.',
    level: 1
  },
  {
    id: (badge_id = badge_id+1),
    name: 'lost-3kg',
    description: 'Você perdeu 3kg. Como prêmio, essa medalha estará pra sempre com você.',
    level: 1
  },
  {
    id: (badge_id = badge_id+1),
    name: 'lost-5kg',
    description: 'Você perdeu 5kg. Como prêmio, essa medalha estará pra sempre com você.',
    level: 2
  },
  {
    id: (badge_id = badge_id+1),
    name: 'lost-10kg',
    description: 'Você perdeu 10kg. Como prêmio, essa medalha estará pra sempre com você.',
    level: 3
  }
].each do |attrs|
  Merit::Badge.create! attrs
end

# Gained Weight
[
  {
    id: (badge_id = badge_id+1),
    name: 'gained-1kg',
    level: 1
  },
  {
    id: (badge_id = badge_id+1),
    name: 'gained-3kg',
    level: 1
  },
  {
    id: (badge_id = badge_id+1),
    name: 'gained-5kg',
    level: 2
  },
  {
    id: (badge_id = badge_id+1),
    name: 'gained-10kg',
    level: 3
  }
].each do |attrs|
  Merit::Badge.create! attrs
end

# Lost IMC Leves
[
  {
    id: (badge_id = badge_id+1),
    name: 'lost-imc',
    level: 2
  },
  {
    id: (badge_id = badge_id+1),
    name: 'ideal-imc',
    level: 3
  }
].each do |attrs|
  Merit::Badge.create! attrs
end

# Workout Levels
[
  {
    id: (badge_id = badge_id+1),
    name: 'workout-addicted',
    description: 'Você malhou 6 dias seguidos na semana. Isso que é força de vontade!',
    level: 2
  },
  {
    id: (badge_id = badge_id+1),
    name: 'workoutholic',
    level: 3
  }
].each do |attrs|
  Merit::Badge.create! attrs
end

# Off Levels
[
  {
    id: (badge_id = badge_id+1),
    name: 'seriously-off',
    level: 1
  },
  {
    id: (badge_id = badge_id+1),
    name: 'highly-off',
    level: 2
  },
  {
    id: (badge_id = badge_id+1),
    name: 'insane-off',
    level: 3
  }
].each do |attrs|
  Merit::Badge.create! attrs
end