require 'uri'
require 'cloudinary/utils'

URI.send :extend, CoreExtensions::URI::Extension
URI.send :extend, CoreExtensions::URI::Protocol
Cloudinary::Utils.send :extend, CoreExtensions::Cloudinary::Utils::ResourceType
Numeric.send :include, CoreExtensions::Numeric::ToF2