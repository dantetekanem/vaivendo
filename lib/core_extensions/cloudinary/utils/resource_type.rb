module CoreExtensions
  module Cloudinary
    module Utils
      module ResourceType
        def supported_video_format?(format)
          Asset::VIDEO_FORMATS.include?(format)
        end

        def resource_type(format)
          extension = ::URI.ext(format)
          case
          when supported_image_format?(extension) then 'image'
          when supported_video_format?(extension) then 'video'
          else 'raw'
          end
        end

        def resource_id(preloaded_file)
          case resource_type(preloaded_file.filename)
          when 'image' then preloaded_file.public_id
          when 'video' then preloaded_file.public_id
          when 'raw' then preloaded_file.filename
          end
        end
      end
    end
  end
end
