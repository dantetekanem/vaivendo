module CoreExtensions
  module Numeric
    module ToF2
      def to_f2
        ("%.2f" % self).to_f
      end
    end
  end
end