module CoreExtensions
  module URI
    module Extension
      def ext(string)
        string.to_s.split('.').last.gsub(/\?.*|\#.*/, '')
      end
    end

    module Protocol
      def add_protocol(string)
        return unless string
        unless string.match(regexp(%w(http https)))
          string = if string.match('//')
            "http:#{ string }"
          else
            "http://#{ string }"
          end
        end
        string
      end
    end
  end
end