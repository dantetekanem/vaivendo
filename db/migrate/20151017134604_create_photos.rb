class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.references :register, index: true, foreign_key: true
      t.integer :position
      t.string :description

      t.timestamps null: false
    end
  end
end
