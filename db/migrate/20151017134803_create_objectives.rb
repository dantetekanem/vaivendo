class CreateObjectives < ActiveRecord::Migration
  def change
    create_table :objectives do |t|
      t.references :user, index: true, foreign_key: true
      t.string :title
      t.string :description
      t.date :start_at
      t.date :goal_at
      t.float :weight_start
      t.float :weight_goal
      t.float :weight_final

      t.timestamps null: false
    end
  end
end
