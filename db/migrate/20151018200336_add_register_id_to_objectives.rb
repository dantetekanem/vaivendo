class AddRegisterIdToObjectives < ActiveRecord::Migration
  def change
    add_reference :objectives, :register, index: true, foreign_key: true
    remove_column :objectives, :start_at
    remove_column :objectives, :weight_start
  end
end
