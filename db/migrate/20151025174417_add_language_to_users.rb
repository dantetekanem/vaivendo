class AddLanguageToUsers < ActiveRecord::Migration
  def change
    add_column :users, :language, :string, default: 'pt-BR'
  end
end
