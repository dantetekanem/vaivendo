class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :type
      t.string :assetable_type
      t.integer :assetable_id
      t.text :source
      t.string :format
      t.integer :width
      t.integer :height
      t.integer :bytes

      t.timestamps null: false
    end
    add_index :assets, :type
    add_index :assets, [:assetable_type, :assetable_id]
    add_index :assets, [:type, :assetable_type, :assetable_id]
  end
end
