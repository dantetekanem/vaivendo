class AddVisualizedToBadges < ActiveRecord::Migration
  def change
    add_column :badges, :viewed, :boolean, default: false
  end
end
