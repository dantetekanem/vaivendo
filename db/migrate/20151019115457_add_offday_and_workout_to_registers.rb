class AddOffdayAndWorkoutToRegisters < ActiveRecord::Migration
  def change
    add_column :registers, :offday, :boolean, default: false
    add_column :registers, :workout, :boolean, default: false
  end
end
