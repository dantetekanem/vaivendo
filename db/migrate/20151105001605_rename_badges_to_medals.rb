class RenameBadgesToMedals < ActiveRecord::Migration
  def change
    rename_table :badges, :medals
  end
end
