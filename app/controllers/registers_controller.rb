class RegistersController < ApplicationController
  def index
    @registers = current_user.registers.oldest.page(params[:page])
    @register = Register.new
  end

  def create
    @register = current_user.registers.new(register_params)
    if @register.save
      redirect_to root_path, notice: 'Registro adicionado com sucesso.'
    else
      redirect_to root_path, notice: @register.errors.full_messages
    end
  end

  def update
    register = current_user.registers.find(params[:id])
    if register.update(register_params)
      render json: register
    else
      render json: { errors: register.errors.full_messages }
    end
  end

  def destroy
    register = current_user.registers.find(params[:id])
    if register.destroy
      redirect_to registers_path, notice: 'Registro removido com sucesso.'
    else
      redirect_to registers_path, notice: register.errors.full_messages
    end
  end

  def graph
  end

  private

  def register_params
    params.require(:register).permit(:measured_at, :weight, :offday, :workout, :note)
  end
end