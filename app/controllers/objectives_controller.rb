class ObjectivesController < ApplicationController
  def index
    @objectives = current_user.objectives.page(params[:page])
    @objective = Objective.new
  end

  def create
    @objective = current_user.objectives.new(objective_params)
    if @objective.save
      redirect_to objectives_path, notice: 'Objetivo criado com sucesso.'
    else
      render :index, alert: @objective.errors.full_messages
    end
  end

  def show
  end

  def new
  end

  def edit
  end

  private

  def objective_params
    params.require(:objective).permit(
      :title,
      :description,
      :register_id,
      :weight_goal,
      :goal_at
    )
  end
end
