class RegistrationsController < Devise::RegistrationsController

  private

  def sign_up_params
    allow = [
      :first_name,
      :last_name,
      :height,
      :gender,
      registers_attributes: [:measured_at, :weight],
      avatar_attributes: [:source, :source_signature]
    ]
    params.require(resource_name).permit(allow)
  end

  def accoutn_update_params
    allow = [
      :first_name,
      :last_name,
      :height,
      :gender,
      :password,
      :password_confirmation,
      :current_password,
      avatar_attributes: [:source, :source_signature]
    ]
    params.require(source_name).permit(allow)
  end

end