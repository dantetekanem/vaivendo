class MedalsController < ApplicationController
  def index
  end

  def show
  end

  def update
    medal = current_user.medals.find(params[:id])
    if medal.update(medal_params)
      render json: { id: medal.id, viewed: medal.viewed, name: medal.name }
    else
      render json: [], status: :unprocessable_entity
    end
  end

  private

  def medal_params
    params.require(:medal).permit(:viewed)
  end
end