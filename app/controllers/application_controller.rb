class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :is_logged?, unless: :devise_controller?
  before_action :set_locale

  private

  def is_logged?
    redirect_to new_user_registration_path unless user_signed_in?
  end

  def set_locale
    I18n.locale = if user_signed_in?
      params[:locale] || current_user.language
    else
      params[:locale] || I18n.default_locale
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [
      :first_name,
      :last_name,
      :height,
      :gender,
      registers_attributes: [:measured_at, :weight],
      avatar_attributes: [:source, :source_signature]
    ]
    devise_parameter_sanitizer.for(:account_update) << [
      :first_name,
      :last_name,
      :height,
      :gender,
      :password,
      :password_confirmation,
      :current_password,
      avatar_attributes: [:source, :source_signature]
    ]
  end
end