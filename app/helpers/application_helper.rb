module ApplicationHelper
  def nav_menu_active(controller)
    controller_name == controller ? 'active' : ''
  end
end
