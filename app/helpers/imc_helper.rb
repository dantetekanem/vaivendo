module ImcHelper
  def imc(weight, height)
    ((1.72 * weight) / (height ** 3.06)).to_f2
  end

  def imc_old(weight, height)
    (weight / (height ** 2)).to_f2
  end

  def imc_situation(imc)
    return I18n.t('imc.level_1') if imc < 17
    return I18n.t('imc.level_2') if imc >= 17 && imc <= 18.49
    return I18n.t('imc.level_3') if imc >= 18.5 && imc <= 24.99
    return I18n.t('imc.level_4') if imc >= 25 && imc <= 29.99
    return I18n.t('imc.level_5') if imc >= 30 && imc <= 34.99
    return I18n.t('imc.level_6') if imc >= 35 && imc <= 39.99
    return I18n.t('imc.level_7') if imc >= 40
  end
end