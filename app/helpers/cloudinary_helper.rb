module CloudinaryHelper
  def start_cloudinary
    content_for(:script) do
      javascript_tag 'CloudinaryHelper.start()'
    end
  end
  def cloudinary_progress
    cloudinary = ''
    cloudinary << content_tag(:div, '', class: 'progress hidden') {
      content_tag(:div, '', class: 'progress-bar', role: 'progressbar', 'aria-valuenow': '0', 'aria-valuemin': '0', 'aria-valuemax': '100')
    }
    cloudinary << content_tag(:div, '', class: 'progress-status hidden text-center')
    cloudinary.html_safe
  end
end
