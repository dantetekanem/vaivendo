require 'active_support/concern'

module Concerns
  module Badges
    extend ActiveSupport::Concern

    included do |base|
      before_save :check_available_badges
    end

    private

    def check_available_badges
      #
    end
  end
end