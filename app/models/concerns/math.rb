module Concerns
  module Math

    def percentage_between(v1, v2)
      decrease = v2 - v1
      r = (decrease / v1) * 100
      ("%2.f" % r).to_f
    end

  end
end