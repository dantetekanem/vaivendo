require 'active_support/concern'

module Concerns
  module CloudinaryAsset
    extend ActiveSupport::Concern

    attr_accessor :source_url, :source_signature

    included do |base|
      before_validation :upload_source_url, :update_preloaded_attributes
      validate :validate_preloaded_file

      def self.style(name, options)
        define_method(name) { style(options.dup) }
      end
    end

    def url(options = {})
      return unless source.present?
      Cloudinary::Utils.cloudinary_url(source, default_url_options.merge(options))
    end

    def to_s
      url
    end

    private

    def style(options)
      url(options)
    end

    def update_attributes_from(upload_hash)
      upload_hash.with_indifferent_access.tap do |upload_hash|
        self.source = upload_hash[:public_id]
        self.format = upload_hash[:format] || upload_hash[:resource_type]
        self.width = upload_hash[:width]
        self.height = upload_hash[:height]
        self.bytes = upload_hash[:bytes]
      end
    end

    def upload_source_url
      return unless source_url.present?
      update_attributes_from Cloudinary::Uploader.upload(
        source_url,
        resource_type: Cloudinary::Utils.resource_type(source_url)
      )
    rescue Exception => e
      errors.add(:source_url, :invalid)
    end

    def update_preloaded_attributes
      return unless preloaded_file
      update_attributes_from Cloudinary::Api.resource(
        Cloudinary::Utils.resource_id(preloaded_file),
        { resource_type: Cloudinary::Utils.resource_type(preloaded_file.filename) }
      )
    end

    def preloaded_file
      return unless source_signature.present?
      @preloaded_file ||= Cloudinary::PreloadedFile.new(source_signature)
    end

    def validate_preloaded_file
      return if preloaded_file.blank? || preloaded_file.valid?
      errors.add(:source, :invalid)
    end

    def resource_type
      Cloudinary::Utils.resource_type(format)
    end

    def default_url_options
      { resource_type: resource_type }
    end
  end
end