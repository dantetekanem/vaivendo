class Objective < ActiveRecord::Base
  include Concerns::Math

  belongs_to :user
  belongs_to :register

  validates :title, presence: true
  validates :user, presence: true
  validates :register, presence: true
  validates :goal_at, presence: true
  validates :weight_goal, presence: true

  scope :completed, -> { where.not(weight_final: nil) }

  def completed?
    weight_final.present?
  end

  def days_missing
    (goal_at - Date.today).to_i
  end

  def days_passed
    (Date.today - register.measured_at).to_i
  end

  def initial_days_missing
    (goal_at - register.measured_at).to_i
  end

  def weight_missing
    (user.weight - weight_goal).to_f2
  end

  def weight_lost
    (register.weight - user.weight).to_f2
  end

  def weight_to_loss
    (register.weight - weight_goal).to_f2
  end

  def daily_weight_to_loss
    (weight_to_loss / days_missing).to_f2
  end

  def weight_expectative_in_days
    return 0.0 if daily_weight <= 0
    (weight_missing / daily_weight).to_i
  end

  def daily_weight
    return 0.0 if weight_lost <= 0
    (weight_lost / days_passed).to_f2
  end

  def percentage_of_loss
    (weight_lost / weight_to_loss * 100).to_f2
  end

  def daily_weight_diff
    return 0.0 if daily_weight <= 0
    percentage_between daily_weight_to_loss, daily_weight
  end
end