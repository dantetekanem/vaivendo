module Merit
  class BadgeRules
    include Merit::BadgeRulesMethods

    def initialize
      grant_on 'registers#create', badge: 'lost-1kg', to: :user do |register|
        register.user.weight_lost >= 1
      end

      grant_on 'registers#create', badge: 'lost-3kg', to: :user do |register|
        register.user.weight_lost >= 3
      end

      grant_on 'registers#create', badge: 'lost-5kg', to: :user do |register|
        register.user.weight_lost >= 5
      end

      grant_on 'registers#create', badge: 'lost-10kg', to: :user do |register|
        register.user.weight_lost >= 10
      end

      grant_on 'registers#update', badge: 'workout-addicted', to: :user do |register|
        register.user.registers.oldest.limit(6).pluck(:workout) == [true, true, true, true, true, true]
      end
    end
  end
end