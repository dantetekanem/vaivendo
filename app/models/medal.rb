class Medal < ActiveRecord::Base
  belongs_to :user

  scope :vieweds, -> { where(viewed: true) }
  scope :unvieweds, -> { where(viewed: false) }

  def viewed!
    update(viewed: true)
  end
end