class User < ActiveRecord::Base
  has_merit

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :registers, dependent: :destroy
  has_many :objectives, dependent: :destroy
  has_many :medals, dependent: :destroy

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :height, presence: true

  has_one :avatar, as: :assetable, class_name: 'Avatar', dependent: :destroy

  accepts_nested_attributes_for :registers
  accepts_nested_attributes_for :avatar, reject_if: -> (attributes) {
    attributes[:source_signature].blank? && attributes[:source].blank?
  }

  enum gender: [:male, :female]

  def name
    "#{ first_name } #{ last_name }"
  end

  def weight
    @weight ||= registers.oldest.first.weight rescue nil
  end

  def register
    registers.oldest.first
  end

  def objective
    @object ||= objectives.last rescue nil
  end

  def imc
    @imc ||= registers.oldest.first.imc rescue nil
  end

  def days
    @days ||= (registers.oldest.first.measured_at - registers.newest.first.measured_at).to_i
  end

  def registered_today?
    registers.where(measured_at: Date.today).exists?
  end

  def weight_loss_average
    @weight_loss_average ||= ((weight_diff > 0 ? weight_diff : weight_diff * -1) / days).to_f2
  end

  def first_weight
    @first_weight ||= registers.newest.first.weight
  end

  def first_imc
    @first_imc ||= registers.newest.first.imc
  end

  def weight_diff
    (weight - first_weight).to_f2
  end

  def weight_lost
    (first_weight - weight).to_f2
  end

  def reverse_registers
    @reverse_registers ||= registers.where('id >= ?', objective.register.id).limit(12).oldest.reverse
  end

  def to_graph_dates
    @to_graph_dates ||= reverse_registers.map(&:measured)
  end

  def to_graph_registers
    @to_graph_registers ||= reverse_registers.map(&:weight)
  end

  def first_register
    @initial_graph_register ||= registers.where('id >= ?', objective.register.id).first
  end

  def expect_weight_of_register(register)
    expected = objective.daily_weight_to_loss * registers.where('id >= ?', register.id).size
    ("%.1f" % (first_weight - expected)).to_f
  end

  def to_graph_expect(register, scale)
    expecs = []
    weight = expect_weight_of_register(register)
    return expecs if !objective
    expecs << weight
    (scale-1).times do
      weight -= objective.daily_weight_to_loss
      expecs << ("%.1f" % weight).to_f
    end
    expecs
  end
end
