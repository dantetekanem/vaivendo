class Avatar < Asset
  include Concerns::CloudinaryAsset

  # Validations
  validates :format, inclusion: { in: Asset::IMAGE_FORMATS }

  # Styles
  style :thumbnail, width: 170, height: 180, crop: :fill
end