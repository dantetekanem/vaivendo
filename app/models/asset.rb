class Asset < ActiveRecord::Base
  IMAGE_FORMATS = %w(jpg jpeg gif png bmp)
  VIDEO_FORMATS = %w(3g2 3gp avi flv mov mp4 mpeg ogv webm wmv)
  OTHER_FORMATS = %w(pdf)

  serialize :info, Hash

  # Relationships
  belongs_to :assetable, polymorphic: true

  # Validations
  validates :source, presence: true

  def file_format
    format == 'raw' ? URI.ext(url) : format
  end
end
