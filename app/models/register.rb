class Register < ActiveRecord::Base
  include Concerns::Badges

  belongs_to :user
  scope :oldest, -> { order('registers.measured_at desc') }
  scope :newest, -> { order('registers.measured_at asc') }

  validates :measured_at, presence: true
  validates :weight, presence: true
  validate :measured_at_cannot_be_repeated, on: :create

  has_many :photos, as: :assetable, class_name: 'Photo', dependent: :destroy

  def previous
    user.registers.oldest.where('id < ?', id).first
  end

  def weight_diff
    return if previous.blank?
    (weight - previous.weight).to_f2
  end

  def diff_direction(diff)
    if diff > 0
      'up'
    elsif diff < 0
      'down'
    else
      'stable'
    end
  end

  def imc
    imc_calc.to_f2
  end

  def imc_calc
    ((1.72 * weight) / (user.height ** 3.06))
  end

  def imc_calc_old
    (weight / (user.height ** 2))
  end

  def imc_diff
    return if previous.blank?
    @imc_diff ||= (imc - previous.imc).to_f2
  end

  def measured
    measured_at.strftime(I18n.t('date.formats.minimum'))
  end

  private

  def measured_at_cannot_be_repeated
    if user.registers.where(measured_at: measured_at).exists?
      errors.add :measured_at, 'Only 1 date allowed for register'
    end
  end
end