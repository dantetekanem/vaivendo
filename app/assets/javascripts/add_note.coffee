$(document).on 'show.bs.modal', '#addNote', (e) ->
	button = $(e.relatedTarget)
	id = button.data('id')
	recipient = button.data('note')
	modal = $(this)
	modal.find('.modal-body textarea').val(recipient)
	modal.find('.btn-save').data('id', id)

$(document).on 'click', '.btn-save', ->
	button = $(this)
	id = button.data('id')
	note = $('.modal-body textarea').val()
	button.text('Salvando...')
	$.ajax
		url: "/registers/#{ id }"
		method: 'PATCH'
		data: {
			register: {
				note: note
			}
		}
	.done (data) ->
		$(".register-#{id}").attr('data-note', note)
		button.text('Salvar')