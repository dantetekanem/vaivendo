initUpdateRegister = ->
  $(document).off 'click', '.update-register'
  $(document).on 'click', '.update-register', ->
    url = $(@).data('url')
    if $(@).hasClass('offday')
      data = { offday: @.checked }
    else
      data = { workout: @.checked }
    $.ajax(
      url: url,
      type: 'PUT',
      data: { register: data }
    )

jQuery(document).on 'page:change', initUpdateRegister