class window.CloudinaryHelper
  constructor: (selector) ->
    @input = $(selector)
    @form = @input.closest('form')
    @progressBarContainer = @input.next()
    @progressBar = @progressBarContainer.find('.progress-bar')
    @progressStatus = @progressBarContainer.next()
    @submitButton = @input.parents('form').find('input[type=submit]')
    @init()

  init: ->
    @input
      .bind('cloudinarystart', (e, data) => @start(e, data))
      .bind('cloudinaryprogress', (e, data) => @progress(e, data))
      .bind('cloudinaryfail', (e, data) => @fail(e, data))
      .bind('cloudinarydone', (e, data) => @done(e, data))
      .cloudinary_fileupload()

  start: (e, data) ->
    @input.hide()
    @submitButton.addClass('disabled')
    @progressStatus.addClass('hidden')
    @progressBarContainer.removeClass('hidden')
    @progressBar
      .css('width', '0')
      .attr('aria-valuenow', 0)
      .text('')
    @progressStatus
      .removeClass('hidden')
      .text('Iniciando o upload...')

  progress: (e, data) ->
    percent = Math.round((data.loaded * 100.0) / data.total)
    @progressStatus.addClass('hidden')
    @progressBar
      .css('width', percent + '%')
      .attr('aria-valuenow', percent)
      .text("#{ filesize(data.loaded) } de #{ filesize(data.total) }")

  fail: (e, data) ->
    @progressStatus
      .removeClass('hidden')
      .text('Upload falhou.')

  done: (e, data) ->
    @progressBarContainer.addClass('hidden')
    @submitButton.removeClass('disabled')
    @progressStatus
      .removeClass('hidden')
      .text('Upload terminou com sucesso.')
    @form.submit() if @form.hasClass('auto-submit-cl')

  @start: ->
    $('input.cloudinary-fileupload[type=file]').each (index, input) ->
      new CloudinaryHelper(input)