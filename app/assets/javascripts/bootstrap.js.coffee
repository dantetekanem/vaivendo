startBootstrap = ->
  $("a[rel~=popover], .has-popover").popover()
  $("a[rel~=tooltip], .has-tooltip").tooltip()
  $('[data-toggle="tooltip"]').tooltip()
jQuery -> startBootstrap()
jQuery(document).on 'page:change', -> startBootstrap()